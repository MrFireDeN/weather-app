package online.beslim.weatherapp;

import android.content.res.Resources;

class CityUtils {

    private final Resources resource;

    CityUtils(Resources resources) {
        this.resource = resources;
    }

    int getImage(String name) {
        if (name.equals(resource.getString(R.string.title_nsk))) {
            return R.drawable.nsk;
        } else if (name.equals(resource.getString(R.string.title_msk))) {
            return R.drawable.msk;
        } else if (name.equals(resource.getString(R.string.title_spb))) {
            return R.drawable.spb;
        } else {
            return R.drawable.ic_launcher_foreground;
        }
    }
}
