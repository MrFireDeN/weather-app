package online.beslim.weatherapp;

import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final List<OneCity> allCity;
    private AllCityFragment allCityFragment;
    private OneCityFragment oneCityFragment;
    private DrawerLayout drawer;

    private TextView textTemperature;
    SensorEventListener listenerTemperature = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Temperature Sensor value = ").append(event.values[0]);
            textTemperature.setText(stringBuilder);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
    private TextView textHumidity;
    SensorEventListener listenerHumidity = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Humidity Sensor value = ").append(event.values[0]);
            textHumidity.setText(stringBuilder);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
    private SensorManager sensorManager;
    private List<Sensor> sensors;

    public MainActivity() {
        allCity = new ArrayList<>();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initCityList();
        initUi();
    }

    private void initCityList() {
        Resources resources = getResources();
        allCity.add(new OneCity(resources.getString(R.string.title_nsk), resources.getString(R.string.description_nsk)));
        allCity.add(new OneCity(resources.getString(R.string.title_msk), resources.getString(R.string.description_msk)));
        allCity.add(new OneCity(resources.getString(R.string.title_spb), resources.getString(R.string.description_spb)));
    }

    private Sensor sensorTemperature;

    private void replaceWithAllCityFragment() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AllCityFragment.CITY_LIST, (Serializable) allCity);
        allCityFragment.setArguments(bundle);
        allCityFragment.setListener(this::replaceWithOneCityFragment);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_container, allCityFragment)
                .commit();
    }

    private void replaceWithOneCityFragment(int id) {
        Bundle args = new Bundle();
        args.putSerializable(OneCityFragment.CITY_ID, allCity.get(id));
        oneCityFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_container, oneCityFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (oneCityFragment.isAdded()) {
            replaceWithAllCityFragment();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        //int id = item.getItemId();
        //switch (id) {
        //    case R.id.nav_nsk:
        //        break;
        //    case R.id.nav_msk:
        //        break;
        //    case R.id.nav_spb:
        //        break;
        //    case R.id.nav_manage:
        //        break;
        //    case R.id.nav_share:
        //        break;
        //    case R.id.nav_send:
        //        break;
        //}

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Sensor sensorHumidity;

    private void initUi() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        allCityFragment = new AllCityFragment();
        oneCityFragment = new OneCityFragment();

        replaceWithAllCityFragment();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener((view) -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());

        sensors();
    }

    private void sensors() {
        textTemperature = findViewById(R.id.textTemperature);
        textHumidity = findViewById(R.id.textHumidity);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        sensorTemperature = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sensorHumidity = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        // Где спрятан TYPE_ABSOLUTE_HUMIDIT?

        sensorManager.registerListener(listenerTemperature, sensorTemperature,
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(listenerHumidity, sensorHumidity,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(listenerTemperature, sensorTemperature);
        sensorManager.unregisterListener(listenerHumidity, sensorHumidity);
    }
}
